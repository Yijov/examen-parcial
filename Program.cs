﻿using System;

namespace examen_parcial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            diasID();
            pagoEnCaja();
            Console.ReadKey(); 
        }
        
        static void diasID()
        {
            Console.WriteLine("Indique un dia de la semana");
            string dia = Console.ReadLine();
            dia=dia.ToUpper();
            if ((dia == "LUNES")||(dia == "MARTES")||(dia == "MIERCOLES")||(dia == "JUEVES")||(dia == "VIERNES")) {
                Console.WriteLine("Este es día laboral de semana");
            } else if ((dia == "SABADO")||(dia == "DOMINGO")){
                Console.WriteLine("Este es día del fin de semana");
            } else {
                Console.WriteLine("Error:" + dia + " no es un dia valido");
            }
        } 
        static void pagoEnCaja()
        {
            Console.Write("Bienvenido. Por favor ingrese el monto a pagar ");
             double precio = Convert.ToDouble(Console.ReadLine());
             Console.Write("¿Por qué Metodo desea parar? Digite T para tarjeta o E para efectivo ");
             string formaDePago = Console.ReadLine();
             formaDePago = formaDePago.ToUpper();

             if (formaDePago == "E")
             {
                 
                 Console.WriteLine("gracias por su compra");

             } else if(formaDePago == "T"){
                 Console.Write("Indique su numero de tarjeta de 12 digitos ");
                 var tarjeta = Console.ReadLine();
                 tarjeta = tarjeta.Replace(" ", "");
                 if ((tarjeta.Length == 12 && EsNumero(tarjeta) && !tarjeta.Contains("-"))){
                     Console.WriteLine("Digite su codigo secreto de tres digitos");
                     Console.ReadLine();
                     Console.WriteLine("Transaccion exitosa. Gracias por su compra");

                 }else {

                     Console.WriteLine("Su numero de tarjeta no es valido. Reinicie transacción");
                 }
                 
             } else {
                 Console.WriteLine("Su metodo de pago no es valido. Reinicie transacción");
                 
             }
        }

        static bool EsNumero (string str){
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                return false;
            }

                return true;
        }
    }
}

